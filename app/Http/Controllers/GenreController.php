<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\Models\Master\Genre;

class GenreController extends Controller
{    

    public function index(Request $request)
    {   
        try {
            $result = Genre::all();

            return response()->json([
                'code'      => 200,
                'results'   => $result,
                'errors'    => null,
            ]);
        } catch (\Exception $e) {
            // $statusCode = ($e->getCode() > 100 && $e->getCode() < 600) ? $e->getCode() : 500;
            return response()->json([
                'code'      => 500,
                'results'   => null,
                'errors'    => $e,
            ]);
        }
    }
}