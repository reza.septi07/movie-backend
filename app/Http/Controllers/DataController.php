<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Services\Master\MasterManager;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
// use EMIS\Fragments\Models\User as EmisUser;
use App\Models\Data\Movie;

class DataController extends Controller
{    
    private $masterService;

    public function __construct()
    {
       
    }

    public function index(Request $request)
    {   
        try {

            $validator = Validator::make($request->all(), [
                'category_id'   => ['integer', 'nullable'],
                'genre_id'      => ['string', 'nullable'],
                'q'             => ['nullable'],
            ]);

            $params         = $validator->validate();
            // $category_id    = $request->category;
            $genre_id       = $request->genre_id;
            $title_like     = $request->q;

            $result = Movie::select('d_movie.*')
                    ->with('genre.master')
                    ->with('category.master')
                    ->with('artist.master')
                    ->with('resolution.master')
                    ->with('resolution.master')
                    ->leftJoin('d_movie_genre', 'd_movie.id', '=', 'd_movie_genre.movie_id')
                    // ->when(!empty($category_id),function($q)use($category_id){
                    //     $q->where('d_movie.category_id', $category_id);
                    // })
                    ->when(!is_null($genre_id),function($q)use($genre_id){
                        $q->where('d_movie_genre.genre_id', $genre_id);
                    })
                    ->when(!is_null($title_like),function($q)use($title_like){
                        $q->where('d_movie.title', 'like', '%'.$title_like.'%');
                    })
                    ->orderBy('d_movie.title','asc')
                    ->distinct()
                    ->get();

            return response()->json([
                'code'      => 200,
                'results'   => $result,
                'errors'    => null,
            ]);
        } catch (ValidationException $e) {

            return response()->json([
                'code'      => 422,
                'results'   => null,
                'errors'    => $e->errors(),
            ]);
        } catch (\Exception $e) {
            // $statusCode = ($e->getCode() > 100 && $e->getCode() < 600) ? $e->getCode() : 500;
            return response()->json([
                'code'      => 500,
                'results'   => null,
                'errors'    => $e->getMessage(),
            ]);
        }
    }

    public function detail($id = null)
    {   
        $result = Movie::where('id', $id)->first();
        try {

            $result = Movie::where('id', $id)->first();

            return response()->json([
                'code'      => 200,
                'results'   => $result,
                'errors'    => null,
            ]);
        } catch (\Exception $e) {
            // $statusCode = ($e->getCode() > 100 && $e->getCode() < 600) ? $e->getCode() : 500;
            return response()->json([
                'code'      => 500,
                'results'   => null,
                'errors'    => $e,
            ]);
        }
    }
}