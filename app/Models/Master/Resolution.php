<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resolution extends Model
{
    use HasFactory;

    protected $table = 'm_resolution';

    protected $fillable = [
        'name',
        'pixel',
    ];
}
