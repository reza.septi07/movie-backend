<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Relation\Movie\MovieGenre;
use App\Models\Relation\Movie\MovieCategory;
use App\Models\Relation\Movie\MovieArtist;
use App\Models\Relation\Movie\MovieResolution;
// use App\Models\Master\Bagian;

class Movie extends Model
{
    use HasFactory;

    protected $table = 'd_movie';

    protected $fillable = [
        'name',
        'path_video',
        'description',
        'duration',
        'director',
        'publication_year',
        'age_rating',
    ];

    public function genre()
    {
        return $this->hasMany(MovieGenre::class, 'movie_id', 'id');
    }

    public function category()
    {
        return $this->hasMany(MovieCategory::class, 'movie_id', 'id');
    }

    public function artist()
    {
        return $this->hasMany(MovieArtist::class, 'movie_id', 'id');
    }

    public function resolution()
    {
        return $this->hasMany(MovieResolution::class, 'movie_id', 'id');
    }
}
