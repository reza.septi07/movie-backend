<?php

namespace App\Models\Relation\Movie;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Resolution;

class MovieResolution extends Model
{
    use HasFactory;

    protected $table = 'd_movie_resolution';

    protected $fillable = [
        'movie_id',
        'resolution_id'
    ];

    public function master()
    {
        return $this->hasOne(Resolution::class, 'id', 'resolution_id');
    }
}
