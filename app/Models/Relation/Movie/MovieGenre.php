<?php

namespace App\Models\Relation\Movie;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Genre;
// use App\Models\Master\Bagian;

class MovieGenre extends Model
{
    use HasFactory;

    protected $table = 'd_movie_genre';

    protected $fillable = [
        'movie_id',
        'genre_id'
    ];

    public function master()
    {
        return $this->hasOne(Genre::class, 'id', 'genre_id');
    }
}
