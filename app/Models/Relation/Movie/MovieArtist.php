<?php

namespace App\Models\Relation\Movie;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Artist;
// use App\Models\Master\Bagian;

class MovieArtist extends Model
{
    use HasFactory;

    protected $table = 'd_movie_artist';

    protected $fillable = [
        'movie_id',
        'artist_id'
    ];

    public function master()
    {
        return $this->hasOne(Artist::class, 'id', 'artist_id');
    }
}
