<?php

namespace App\Models\Relation\Movie;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Category;
// use App\Models\Master\Bagian;

class MovieCategory extends Model
{
    use HasFactory;

    protected $table = 'd_movie_category';

    protected $fillable = [
        'movie_id',
        'category_id'
    ];

    public function master()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
