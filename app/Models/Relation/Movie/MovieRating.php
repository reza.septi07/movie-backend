<?php

namespace App\Models\Relation\Movie;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MovieRating extends Model
{
    use HasFactory;

    protected $table = 'd_movie_rating';

    protected $fillable = [
        'user_id',
        'movie_id',
        'rating'
    ];
}
