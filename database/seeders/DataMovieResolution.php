<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Relation\Movie\MovieResolution;

class DataMovieResolution extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = array();

        for ($i=1; $i <= 20; $i++) { 
            
            for ($j=1; $j <= 5; $j++) { 
                $rand = rand(1, 6);
                
                if(@$data['resolution_id'] == $rand){
                    $j--;
                    continue;
                }

                $data = array(
                    'movie_id'          => $i,
                    'resolution_id'     => $rand,
                );

                array_push($values, $data);
            }
        }

        MovieResolution::insert($values);
    }
}
