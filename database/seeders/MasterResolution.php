<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Master\Resolution;

class MasterResolution extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = array(
            array(
                'name'  => 'SD',
                'pixel' => 480,
            ),
            array(
                'name'  => 'HD',
                'pixel' => 720,
            ),
            array(
                'name'  => 'HD',
                'pixel' => 1080,
            ),
            array(
                'name'  => 'FHD',
                'pixel' => 1080,
            ),
            array(
                'name'  => 'QHD',
                'pixel' => 1440,
            ),
            array(
                'name'  => 'UHD',
                'pixel' => 2160,
            ),
        );

        Resolution::insert($values);
    }
}
