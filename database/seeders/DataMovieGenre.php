<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Relation\Movie\MovieGenre;

class DataMovieGenre extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = array();

        for ($i=1; $i <= 20; $i++) { 
            
            for ($j=1; $j <= 3; $j++) { 
                $rand = rand(1, 6);
                
                if(@$data['genre_id'] == $rand){
                    $j--;
                    continue;
                }

                $data = array(
                    'movie_id'      => $i,
                    'genre_id'      => $rand,
                );

                array_push($values, $data);
            }
        }

        MovieGenre::insert($values);
    }
}
