<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Relation\Movie\MovieCategory;

class DataMovieCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = array();

        for ($i=1; $i <= 20; $i++) { 
            
            for ($j=1; $j <= 3; $j++) { 
                $rand = rand(1, 5);
                
                if(@$data['category_id'] == $rand){
                    $j--;
                    continue;
                }

                $data = array(
                    'movie_id'      => $i,
                    'category_id'   => $rand,
                );

                array_push($values, $data);
            }
        }

        MovieCategory::insert($values);
    }
}
