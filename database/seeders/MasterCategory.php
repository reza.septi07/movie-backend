<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Master\Category;

class MasterCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = array(
            array('name' => 'Seru'),
            array('nama' => 'Kelam'),
            array('nama' => 'Mendebarkan'),
            array('nama' => 'Menyenangkan'),
            array('nama' => 'Mengharukan'),
        );

        Category::insert($values);
    }
}
