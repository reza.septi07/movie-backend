<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Relation\Movie\MovieRating;

class DataMovieRating extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = array();

        for ($i=1; $i <= 20; $i++) { 
            
            for ($j=1; $j <= 4; $j++) { 
                $rand = rand(1, 5);

                $data = array(
                    'user_id'       => $j,
                    'movie_id'      => $i,
                    'rating'        => $rand,
                );

                array_push($values, $data);
            }

        }

        MovieRating::insert($values);
    }
}
