<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MasterCategory::class);
        $this->call(MasterArtist::class);
        $this->call(MasterGenre::class);
        $this->call(MasterResolution::class);
        $this->call(DataMovie::class);
        $this->call(DataMovieRating::class);
        $this->call(DataMovieGenre::class);
        $this->call(DataMovieCategory::class);
        $this->call(DataMovieArtist::class);
        $this->call(DataMovieResolution::class);
    }
}
