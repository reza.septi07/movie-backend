<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Master\Genre;

class MasterGenre extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            'Fiksi Ilmiah',
            'Laga & Perteluangan',
            'Adaptasi Komik',
            'Romantis',
            'Komedi',
            'Horor',
        );

        $values = array();

        foreach($data as $value){
            $arr_data = array(
                'name'  => $value,
                'slug'  => strtolower(str_replace(" ", "-", $value))
            );

            array_push($values, $arr_data);
        }

        Genre::insert($values);
    }
}
