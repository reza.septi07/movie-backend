<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Data\Movie;

class DataMovie extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = array();

        for ($i=1; $i <= 10; $i++) { 
            
            $data = array(
                'title'             => 'Down In The Jungle, Peek a Boo, Zigaloo + More Nursery Rhymes '.$i,
                'path_video'        => '1sP8M7bFg-4',
                'description'       => 'LooLoo Kids is the place where children find all their favorite nursery rhymes and songs with lyrics. Johnny, Johnny, Yes Papa, The Wheels On The Bus, Peek-a Boo and hundreds of beautiful educational songs are entertaining kids around the world.  The best English learning lessons for kids are now the Musical Adventures of Johnny & Friends. They sing, dance and learn while having a lot of fun. Children are learning to count, to recognize colors, to spell the Alphabet. Rhyming is the new flavour of learning!',
                'duration'          => 4600,
                'director'          => 'LooLoo Kids',
                'publication_year'  => 2020,
                'age_rating'        => 5,
            );

            array_push($values, $data);

            $data = array(
                'title'             => 'Baby Shark Turned into a Cube! '.$i,
                'path_video'        => 'Jy3Et0i8HUU',
                'description'       => 'Have fun with Baby Shark and the Shark Family! You\'re watching "Baby Shark Turned into a Cube!", a super fun Baby Shark Toy and Sing Along compilation only on Baby Shark Official channel!',
                'duration'          => 5500,
                'director'          => 'Thomas',
                'publication_year'  => 2021,
                'age_rating'        => 5,
            );

            array_push($values, $data);
        }

        Movie::insert($values);
    }
}
