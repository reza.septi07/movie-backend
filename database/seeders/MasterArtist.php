<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Master\Artist;

class MasterArtist extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = array(
            array('name' => 'Julian Robert'),
            array('nama' => 'Kevin'),
            array('nama' => 'Aulia'),
            array('nama' => 'Bambang'),
            array('nama' => 'Tousen Keeve'),
        );

        Artist::insert($values);
    }
}
