<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDMovie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_movie', function (Blueprint $table) {
            $table->id();
            $table->string('title', 200)->nullable();
            $table->string('path_video', 250)->nullable();
            $table->text('description')->nullable();
            $table->integer('duration')->default(0)->comment('Number in minute');
            $table->string('director', 100)->nullable();
            $table->year('publication_year')->nullable();
            $table->integer('age_rating')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_movie');
    }
}
